# 员工管理后台系统


### 数据库配置
- 配置数据库连接文件在server/db/db.js 数据库账号和密码要与你本地的一致
- 建议使用 Navicat 数据库可视化软件创建MySQL数据库：worker（与server/db/db.js配置的一致即可）
- 然后将worker.sql导入创建的数据库中

### 运行项目
- 推荐使用vscode编译器 其他编译器也行
- 在终端输入cd命令进入server目录： npm install 安装依赖  安装完成后，在该目录输入npm start即可运行
- 在终端输入cd命令进入client目录： npm install 安装依赖  安装完成后，在该目录输入npm start即可运行


